import React from 'react';

class TrafficLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btnTxt: 'Clear',
      logs: this.props.logs
    };
    this.clear = this.clear.bind(this);
  }

  clear() {
    return this.setState({logs: []});
  }

  render() {
    return (
      <div className="wrapper wrapper-traffic-log">
        <div className="row">
          <div className="logger">
            <table className="table table-logger">
              <tbody>
              {this.state.logs.map(function (log, index) {
                return <tr key={index}>
                  <td className="time">{log.time}</td>
                  <td>:&nbsp;</td>
                  <td className="event">{log.event}</td>
                </tr>
              })}
              </tbody>
            </table>
          </div>
        </div>
        <div className="row">
          <button className="btn btn-primary" onClick={this.clear}>{this.state.btnTxt}</button>
        </div>
      </div>
    );
  }
}

export default TrafficLog;