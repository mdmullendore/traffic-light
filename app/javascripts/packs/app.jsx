import React from 'react';
import ReactDOM from 'react-dom';
import Main from './main';

document.addEventListener('DOMContentLoaded', function () {
  ReactDOM.render(
    <Main />,
    document.body.appendChild(document.createElement('div')),
  )
});
