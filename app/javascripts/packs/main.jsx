import React from 'react';
import TrafficLight from './traffic-light.jsx';
import TrafficLog from './traffic-log.jsx';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Traffic Light',
      logs: []
    };
    this.log = this.log.bind(this);
  }

  log(log) {
    let logs = this.state.logs;
    logs.push(log);
    return this.setState({logs: logs});
  }

  render() {
    return (
      <section className="container">
        <h1>{this.state.title}</h1>
        <TrafficLight logs={this.state.logs} log={this.log}> </TrafficLight>
        <TrafficLog logs={this.state.logs} log={this.log}> </TrafficLog>
      </section>
    );
  }
}

export default Main;