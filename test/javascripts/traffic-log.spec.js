import React from 'react';
import ReactDOM from 'react-dom';

import { expect } from 'chai';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import TrafficLog from '../../app/javascripts/packs/traffic-log.jsx';

describe('<TrafficLog/>', function () {
  const wrapper = shallow(<TrafficLog logs={[]}/>);

  it('should have btn', function () {
    expect(wrapper.find('.btn')).to.have.length(1);
  });

  it('should clear logs', function () {
    wrapper.find('.btn').simulate('click');
    expect(wrapper.state().logs).to.deep.equal([]);
  });

  it('should have empty logs', function () {
    expect(wrapper.state().logs).to.deep.equal([]);
  });

  it('should have logs', function () {
    let log = {time: new Date(), event: 'test event'};
    wrapper.setState({ logs: [log] });
    expect(wrapper.state().logs).to.deep.equal([log]);
  });

  it('should have btnText', function () {
    expect(wrapper.state().btnTxt).to.equal('Clear');
  });

});