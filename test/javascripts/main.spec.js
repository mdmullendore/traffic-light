import React from 'react';
import ReactDOM from 'react-dom';

import { expect } from 'chai';
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import Main from '../../app/javascripts/packs/main.jsx';

describe('<Main/>', function () {
  const wrapper = shallow(<Main/>);

  it('should have h1', function () {
    expect(wrapper.find('h1')).to.have.length(1);
  });

  it('should have title', function () {
    expect(wrapper.state().title).to.equal('Traffic Light');
  });

  it('should have Traffic Light', function () {
    expect(wrapper.find('TrafficLight')).to.have.length(1);
  });

  it('should have Traffic Log', function () {
    expect(wrapper.find('TrafficLog')).to.have.length(1);
  });

  it('should be empty logs', function () {
    expect(wrapper.state().logs).to.deep.equal([]);
  });

  it('should have logs', function () {
    let log = {time: new Date(), event: 'test event'};
    wrapper.setState({ logs: [log] });
    expect(wrapper.state().logs).to.deep.equal([log]);
  });
});