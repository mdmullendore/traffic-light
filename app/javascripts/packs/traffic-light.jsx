import React from 'react';

class TrafficLight extends React.Component {
  constructor(props) {
    super(props);
    this.lights = [
      {color: 'red', status: 'active'},
      {color: 'yellow', status: 'deactive'},
      {color: 'green', status: 'deactive'}
    ];
    this.state = {
      btnTxt: 'Reset',
      lights: this.lights,
      interval: null,
      count: null
    };
    this.reset = this.reset.bind(this);
    this.log = this.props.log.bind(this);
  }

  componentDidMount() {
    this.startLightInterval();
    this.logEvent(`red light change. light duration ${this.lightDurations('red')}`);
  }

  activeIndex(obj) {
    return obj.findIndex(function (light) {
      return light.status === 'active';
    });
  }

  clearLightInterval() {
    let self = this;
    return clearInterval(self.state.interval);
  }

  deactivateLights() {
    this.setState({
      lights: [
        {color: 'red', status: 'deactive'},
        {color: 'yellow', status: 'deactive'},
        {color: 'green', status: 'deactive'},
      ]
    })
  }

  findActiveLight(obj) {
    return obj.find(function (light) {
      return light.status === 'active';
    }).color;
  }

  lightDurations(color) {
    switch (color) {
      case 'red':
        return 3;
      case 'yellow':
        return 2;
      default:
        return 1;
    }
  }

  logEvent(log) {
    this.log({
      time: new Date().toString(),
      event: log
    });
  }

  nextIndex(number) {
    if (number < 0 || number === 2) {
      return 0;
    }
    return number + 1;
  }

  nextLight(nextIndex) {
    let lights = this.state.lights.map(function (light, index) {
      if (index === nextIndex) {
        return {color: light.color, status: 'active'};
      }
      return light;
    });
    return this.setState({lights: lights});
  }

  reset() {
    this.logEvent('reset traffic light');
    this.clearLightInterval();
    this.resetLights();
    return this.startLightInterval();
  }

  resetLights() {
    this.setState({lights: this.lights});
    return this.logEvent(`red light change. light duration ${this.lightDurations('red')}`);
  }

  startLightInterval() {
    let self = this;
    this.setState({count: 0});
    this.state.interval = setInterval(function () {
      self.state.count = self.state.count + 1;
      for (let i = 0; i < self.state.lights.length; i++) {
        if (self.findActiveLight(self.state.lights) === self.state.lights[i].color && self.state.count === self.lightDurations(self.findActiveLight(self.state.lights))) {
          let nextIndex = self.nextIndex(self.activeIndex(self.state.lights));
          self.deactivateLights();
          self.nextLight(nextIndex);
          self.logEvent(`${self.findActiveLight(self.state.lights)} light change. light duration ${self.lightDurations(self.findActiveLight(self.state.lights))}`);
          return self.setState({count: 0});
        }
      }
    }, 1000);
  }

  render() {
    return (
      <div className="wrapper wrapper-traffic-light">
        <div className="row">
          <div className="traffic-light-component traffic-light-bg">
            {this.state.lights.map(function (light, index) {
              return <div key={index} className={'light' + ' ' + light.color + '-light' + ' ' + light.status}> </div>;
            })}
          </div>
        </div>
        <div className="row">
          <button className="btn btn-primary" onClick={this.reset}>{this.state.btnTxt}</button>
        </div>
      </div>
    );
  }
}

export default TrafficLight;