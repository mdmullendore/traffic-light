# README

This README will instruct you on setting up project, running the web server and testing the scripts.
Open up your terminal and run the commands in the 'traffic-light' directory.

## TECH STACK

* Ruby
* Rails
* React-Rails
* Webpacker
* React
* Babel
* Jest
* Chai
* Enzyme


## TECH STACK NOTES

This project requires **ruby** version:

####**ruby-2.3.0**

This project requires **node** version:

####**v11.1.0**

This project requires **yarn** version:

####**v1.12.1**


## SET UP

Install gems dependencies by running:

```
 bundle install
```

Install node dependencies by running:

```
 yarn
```


## RUNNING THE SERVER

Start rails by running:

```
 bin/rails s
```

Note - this may take a moment; webpacker will have to compile scripts (see tmp, log and public directories created).


## RUNNING UNIT TESTS

Run test:

```
 yarn test
```

## Application Structure

Notes on where important things live.


* [app](./app)
    * [assets](./assets) 
       * [stylesheets](./assets/stylesheets)
           * [sass](./assets/stylesheets/sass) - includes app styles
    * [controllers](./assets/controllers) - includes main controller to render index
    * [javascripts](./assets/javascripts)
       * [path](./assets/javascripts/path) - includes react scripts to render ui
    * [views](./assets/views)
       * [main](./assets/views/main) - includes index to render react ui
* [test](./file_in_root.ext)
    * [javascripts](./javascripts) - tests to test react components
* [.babelrc](./.babelrc)
* [Gemfile](./Gemfile)
* [package.json](./package.json)
* [README.md](./README.md)
* [yarn.lock](./yarn.lock)
