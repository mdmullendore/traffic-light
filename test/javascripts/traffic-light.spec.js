import React from 'react';
import ReactDOM from 'react-dom';

import { expect } from 'chai';
import jest from 'jest-mock';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import TrafficLight from '../../app/javascripts/packs/traffic-light.jsx';

describe('<TrafficLight/>', function () {
  let lights = [
    {color: 'red', status: 'active'},
    {color: 'yellow', status: 'deactive'},
    {color: 'green', status: 'deactive'}
  ];

  let log = jest.fn();
  const wrapper = shallow(<TrafficLight logs={[]} log={log}/>);

  it('should have btn', function () {
    expect(wrapper.find('.btn')).to.have.length(1);
  });

  it('should have btnText', function () {
    expect(wrapper.state().btnTxt).to.equal('Reset');
  });

  it('should lights state', function () {
    expect(wrapper.state().lights).to.deep.equal(lights);
  });

  it('should reset lights', function () {
    wrapper.find('.btn').simulate('click');
    expect(wrapper.state().lights).to.deep.equal(lights);
  });

});